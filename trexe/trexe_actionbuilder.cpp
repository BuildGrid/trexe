/*
 * Copyright 2022 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <trexe_actionbuilder.h>

#include <buildboxcommon_logging.h>
#include <buildboxcommon_mergeutil.h>
#include <buildboxcommon_stringutils.h>
#include <filesystem>
#include <regex>

namespace trexe {

static std::pair<std::shared_ptr<Digest>, std::shared_ptr<Command>>
generateCommand(const std::vector<std::string> &argv,
                const std::map<std::string, std::string> &environment,
                const std::set<std::string> &outputPaths,
                const std::set<std::pair<std::string, std::string>> &platform,
                const std::string &workingDir,
                const std::set<std::string> &outputNodeProperties)
{
    BUILDBOX_LOG_DEBUG("Generating command.");
    auto command = std::make_shared<Command>();
    for (const auto &arg : argv) {
        command->add_arguments(arg);
    }

    // environment variables
    for (const auto &var : environment) {
        auto envVar = command->add_environment_variables();
        envVar->set_name(var.first);
        envVar->set_value(var.second);
    }

    for (const auto &path : outputPaths) {
        command->add_output_paths(path);
    }

    // platform (deprecated but set to the same value as the Action for
    // redundancy)
    Platform *commandPlatform = command->mutable_platform();
    for (const auto &p : platform) {
        auto property = commandPlatform->add_properties();
        property->set_name(p.first);
        property->set_value(p.second);
    }

    command->set_working_directory(workingDir);

    // ouptut_node_properties
    for (const auto &property : outputNodeProperties) {
        command->add_output_node_properties(property);
    }

    auto digest =
        std::make_shared<Digest>(CASHash::hash(command->SerializeAsString()));

    BUILDBOX_LOG_DEBUG("Generated Command: Digest:"
                       << toString(*digest)
                       << ", Command:" << command->SerializeAsString());

    return std::make_pair<>(digest, command);
}

// Create a chain of directories,
// returning the head and the pointer to the tail
static std::pair<std::shared_ptr<NestedDirectory>, NestedDirectory *>
createWrappingDirectories(const std::filesystem::path &path)
{
    auto head = std::make_shared<NestedDirectory>();
    NestedDirectory *tail = head.get();
    for (const auto &part : path) {
        const auto partStr = part.native().c_str();
        (*tail->d_subdirs)[partStr] = NestedDirectory();
        tail = &tail->d_subdirs->at(partStr);
    }

    return {head, tail};
}

// Generates input tree and returns everything needed for
// FMB and uploads
// namely:
// - tree digest
// - digestToDiskPath (all needed file digests to path map)
// - digestToDirMessages (all needed dir proto digests to serialized proto map)
static std::tuple<std::shared_ptr<Digest>, std::shared_ptr<digest_string_map>,
                  std::shared_ptr<digest_string_map>>
generateInputTree(std::shared_ptr<CASClient> casClient,
                  const std::shared_ptr<Digest> inputRootDigest,
                  const std::vector<InputPathOption> &inputPaths,
                  const bool followSymlinks)
{
    BUILDBOX_LOG_DEBUG("Generating input merkle tree.");

    std::shared_ptr<std::unordered_map<buildboxcommon::Digest, std::string>>
        digestToDiskPath = std::make_shared<
            std::unordered_map<buildboxcommon::Digest, std::string>>();
    std::shared_ptr<digest_string_map> digestToSerializedProtos =
        std::make_shared<
            std::unordered_map<buildboxcommon::Digest, std::string>>();

    // First handle shortcuts: input root without paths, no inputs at all.

    if (inputRootDigest && inputPaths.empty()) {
        return std::make_tuple(inputRootDigest, digestToDiskPath,
                               digestToSerializedProtos);
    }

    if (!inputRootDigest && inputPaths.empty()) {
        NestedDirectory emptyDir;
        auto emptyDigest = std::make_shared<Digest>(
            emptyDir.to_digest(digestToSerializedProtos.get()));

        return std::make_tuple(emptyDigest, digestToDiskPath,
                               digestToSerializedProtos);
    }

    std::vector<std::vector<Directory>> treesToMerge;

    if (inputRootDigest) {
        treesToMerge.push_back(casClient->getTree(*inputRootDigest));
    }

    for (const auto &input : inputPaths) {
        const auto &[localInput, remoteInput] = input;
        const auto localInputPath = std::filesystem::path(localInput);
        // If no remote mapping, the remote path is the filename
        // Implementation note: don't use move constructor of path
        // https://gitlab.com/BuildGrid/trexe/-/merge_requests/56
        std::filesystem::path remoteInputPath =
            remoteInput.has_value() ? std::filesystem::path(*remoteInput)
                                          .relative_path()
                                          .lexically_normal()
                                    : localInputPath.filename();
        // remove the trailing slash of a directory path
        if (!remoteInputPath.has_filename()) {
            remoteInputPath = remoteInputPath.parent_path();
        }

        auto [nestedDir, leafDir] =
            createWrappingDirectories(remoteInputPath.parent_path());
        const auto inputStatus = std::filesystem::status(localInputPath);
        if (!std::filesystem::exists(inputStatus)) {
            BUILDBOXCOMMON_THROW_EXCEPTION(
                std::runtime_error, "Input doesn't exist: " << localInputPath);
        }
        if (std::filesystem::is_directory(inputStatus)) {
            if (remoteInput.has_value() && remoteInputPath != ".") {
                (*leafDir->d_subdirs)[remoteInputPath.filename()] =
                    make_nesteddirectory(localInputPath.c_str(),
                                         digestToDiskPath.get(),
                                         followSymlinks);
            }
            else {
                leafDir = nullptr;
                nestedDir =
                    std::make_shared<NestedDirectory>(make_nesteddirectory(
                        localInputPath.c_str(), digestToDiskPath.get(),
                        followSymlinks));
            }
        }
        else if (std::filesystem::is_regular_file(inputStatus)) {
            const auto file = File(localInputPath.c_str());
            leafDir->d_files[remoteInputPath.filename()] = file;
            (*digestToDiskPath)[file.d_digest] = localInputPath.c_str();
        }
        else if (std::filesystem::is_symlink(inputStatus)) {
            leafDir->d_symlinks[remoteInputPath.filename()] =
                std::filesystem::read_symlink(localInputPath);
        }
        else {
            BUILDBOXCOMMON_THROW_EXCEPTION(
                std::runtime_error,
                "File type is not supported to be an input: "
                    << localInputPath);
        }
        std::vector<Directory> treeDirectory;
        Tree rootTree = nestedDir->to_tree();
        treeDirectory.push_back(rootTree.root());
        for (const Directory child : rootTree.children()) {
            treeDirectory.push_back(child);
        }
        treesToMerge.push_back(treeDirectory);
    }

    Digest rootDigest;
    MergeUtil::DigestVector mergedDirList;
    const bool result = MergeUtil::createMergedDigest(
        treesToMerge, &rootDigest, digestToSerializedProtos.get(),
        &mergedDirList);
    if (!result) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::runtime_error,
            "Unable to merge input-path trees because of collision");
    }

    return std::make_tuple(std::make_shared<Digest>(rootDigest),
                           digestToDiskPath, digestToSerializedProtos);
}

std::pair<std::shared_ptr<Digest>, std::shared_ptr<Action>>
generateAction(const Digest &commandDigest, const Digest &inputRootDigest,
               const int &execTimeout, const bool doNotCache,
               const std::string &salt,
               const std::set<std::pair<std::string, std::string>> &platform)
{
    BUILDBOX_LOG_DEBUG("Generating Action: ");

    auto action = std::make_shared<Action>();
    action->mutable_command_digest()->CopyFrom(commandDigest);

    // input_root_digest
    action->mutable_input_root_digest()->CopyFrom(inputRootDigest);

    // timeout
    if (execTimeout != 0) { // optional, has server default if not specified
        google::protobuf::Duration actionTimeout;
        actionTimeout.set_seconds(execTimeout);
        action->mutable_timeout()->CopyFrom(actionTimeout);
    }

    // do_not_cache
    action->set_do_not_cache(doNotCache);

    // salt
    if (salt != "") {
        action->set_salt(salt);
    }

    // platform
    Platform *actionPlatform = action->mutable_platform();
    for (const auto &p : platform) {
        auto property = actionPlatform->add_properties();
        property->set_name(p.first);
        property->set_value(p.second);
    }

    auto digest =
        std::make_shared<Digest>(CASHash::hash(action->SerializeAsString()));

    return std::make_pair<>(digest, action);
}

ActionData buildAction(
    std::shared_ptr<CASClient> casClient, const std::vector<std::string> &argv,
    const std::string &workingDir,
    const std::vector<InputPathOption> &inputPaths,
    const std::shared_ptr<Digest> inputRootDigest,
    const std::set<std::string> &outputPaths,
    const std::set<std::pair<std::string, std::string>> &platform,
    const std::map<std::string, std::string> &environment,
    const int &execTimeout, const bool doNotCache, const bool followSymlinks,
    const std::string &salt, const std::set<std::string> &outputNodeProperties)
{
    const auto commandData =
        generateCommand(argv, environment, outputPaths, platform, workingDir,
                        outputNodeProperties);

    const auto inputTreeData = generateInputTree(casClient, inputRootDigest,
                                                 inputPaths, followSymlinks);

    const auto inputTreeDigest = std::get<0>(inputTreeData);

    BUILDBOX_LOG_DEBUG("Generated input root: "
                       << inputTreeDigest->hash() << "/"
                       << inputTreeDigest->size_bytes());

    // check that these digests are actually generated
    const auto actionData =
        generateAction(*commandData.first, *inputTreeDigest, execTimeout,
                       doNotCache, salt, platform);

    const auto actionDigest = actionData.first;
    const auto actionProto = actionData.second;

    BUILDBOX_LOG_DEBUG("Generated action: Digest: "
                       << toString(*actionDigest)
                       << ", Action:" << actionProto->SerializeAsString());

    return ActionData{
        .d_commandProto = commandData.second,
        .d_commandDigest = commandData.first,
        .d_actionProto = actionProto,
        .d_actionDigest = actionDigest,
        .d_inputDigestsToPaths = std::get<1>(inputTreeData),
        .d_inputDigestsToSerializedProtos = std::get<2>(inputTreeData),
    };
}

} // namespace trexe
