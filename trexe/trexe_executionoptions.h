/*
 * Copyright 2022 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_TREXE_EXECUTIONOPTIONS
#define INCLUDED_TREXE_EXECUTIONOPTIONS

#include <memory>
#include <ostream>
#include <set>
#include <string>
#include <unordered_map>

#include <buildboxcommon_commandline.h>
#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_logging.h>
#include <toml++/toml.h>

#include <trexe_cmdlinespec.h>

namespace trexe {

const std::string TREXE_METADATA_TOOL_NAME = "trexe";

#ifndef TREXE_VERSION
#error "TREXE_VERSION is not defined"
#else
const std::string TREXE_VERSION_EO = TREXE_VERSION; // set by CMake
#endif

/**
 * What will trexe do, determined by the combination of options
 */
enum class TrexeMode {
    // Cancel the async operation
    CANCEL,
    // Download the results from the async operation
    DOWNLOAD,
    // Run execution locally with caching
    CACHE_ONLY_EXECUTION,
    // Submit a remote execution and wait until it's finished
    REMOTE_EXECUTION_BLOCKING,
    // Submit a remote execution and print the operation-id to stdout
    REMOTE_EXECUTION_ASYNC,
};

struct InputPathOption {
    InputPathOption(const std::string &local) : d_local(local) {}
    InputPathOption(const std::string &local, const std::string &remote)
        : d_local(local), d_remote(remote)
    {
    }
    std::string d_local;
    std::optional<std::string> d_remote = {};

    InputPathOption static splitLocalRemote(const std::string &s);

    bool operator==(const InputPathOption &other) const;
};

std::ostream &operator<<(std::ostream &os,
                         const InputPathOption &inputPathOption);

struct ExecutionOptions {

    ExecutionOptions()
    {
        d_priority = std::make_shared<int>(0);
        d_metadata["tool-name"] = TREXE_METADATA_TOOL_NAME;
        d_metadata["tool-version"] = TREXE_VERSION_EO;
        // TODO: refactor so empty strings don't have to be prepopulated
        d_metadata["tool-invocation-id"] = "";
        d_metadata["correlated-invocations-id"] = "";
    }

    /**
     * Check that the combination of options are valid.
     *
     * Throws an `std::runtime` exception if options are invalid.
     */
    void validate() const;

    /**
     * Get the trexe mode
     */
    TrexeMode mode() const;

    bool d_cancelMode = false;
    bool d_blockingExecute = true;
    bool d_cacheOnly = false;
    bool d_skipCacheLookup = false;
    bool d_doNotCache = false;
    bool d_followSymlinks = true; // default to true for backward compatibility
    buildboxcommon::LogLevel d_logLevel = buildboxcommon::LogLevel::ERROR;
    std::string d_downloadResultsPath; // do not download when empty
    std::vector<std::string> d_argv;
    std::string d_workingDir = ".";
    std::vector<InputPathOption> d_inputPaths;
    std::string d_inputRootDigest;
    std::set<std::string> d_outputPaths;
    std::set<std::string> d_outputNodeProperties;
    std::string d_operation;
    std::set<std::pair<std::string, std::string>> d_platform;
    std::map<std::string, std::string> d_environment;
    int d_execTimeout = 0;
    std::string d_salt;
    // TODO: why is this a pointer?
    std::shared_ptr<int> d_priority;
    std::unordered_map<std::string, std::string> d_metadata;
    std::string d_resultMetadataFile;
    std::string d_actionResultJsonFile;
    std::string d_runnerCommand = "buildbox-run";
    std::string d_stdoutFile;
    std::string d_stderrFile;
    std::vector<std::string> d_extraRunArgs;
    bool d_streamLogs = false;
    // REAPI connections
    buildboxcommon::ConnectionOptions d_execConn;
    buildboxcommon::ConnectionOptions d_casConn;
    buildboxcommon::ConnectionOptions d_acConn;
    buildboxcommon::ConnectionOptions d_lsConn;

    static ExecutionOptions
    fromCommandLine(const std::vector<std::string> &,
                    const buildboxcommon::CommandLine &);

    static ExecutionOptions newFromTOML(const toml::table &table);
    static void updateFromTOML(const toml::table &table,
                               ExecutionOptions &options);

    friend std::ostream &operator<<(std::ostream &, ExecutionOptions);
};

} // namespace trexe

#endif
