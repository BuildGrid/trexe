/*
 * Copyright 2022 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <sstream>

#include <buildboxcommon_logging.h>
#include <buildboxcommon_logging_commandline.h>
#include <trexe_cmdlinespec.h>

namespace trexe {

using ArgumentSpec = buildboxcommon::CommandLineTypes::ArgumentSpec;
using DataType = buildboxcommon::CommandLineTypes::DataType;
using TypeInfo = buildboxcommon::CommandLineTypes::TypeInfo;
using DefaultValue = buildboxcommon::CommandLineTypes::DefaultValue;

CmdLineSpec::CmdLineSpec(
    buildboxcommon::ConnectionOptionsCommandLine const &connectionOptionsSpec,
    buildboxcommon::ConnectionOptionsCommandLine const
        &connectionOptionsCasSpec,
    buildboxcommon::ConnectionOptionsCommandLine const
        &connectionOptionsAcSpec,
    buildboxcommon::ConnectionOptionsCommandLine const
        &connectionOptionsExecSpec,
    buildboxcommon::ConnectionOptionsCommandLine const
        &connectionOptionsLogStreamSpec)
{
    d_spec.emplace_back("help", "Display usage and exit.",
                        TypeInfo(DataType::COMMANDLINE_DT_BOOL));

    d_spec.insert(d_spec.end(), connectionOptionsSpec.spec().cbegin(),
                  connectionOptionsSpec.spec().cend());

    d_spec.insert(d_spec.end(), connectionOptionsCasSpec.spec().cbegin(),
                  connectionOptionsCasSpec.spec().cend());

    d_spec.insert(d_spec.end(), connectionOptionsAcSpec.spec().cbegin(),
                  connectionOptionsAcSpec.spec().cend());

    d_spec.insert(d_spec.end(), connectionOptionsExecSpec.spec().cbegin(),
                  connectionOptionsExecSpec.spec().cend());

    d_spec.insert(d_spec.end(), connectionOptionsLogStreamSpec.spec().cbegin(),
                  connectionOptionsLogStreamSpec.spec().cend());

    d_spec.emplace_back("config-file",
                        "Config file to specify all options in TOML.",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back("d",
                        "Download completed action outputs to this path. "
                        "Leave empty for no download.",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back("working-dir", "Path to working directory.",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back("input-root-digest", "Input root digest for Action. ",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back("input-path",
                        "Input path for Action. "
                        "--input-path=<path> for each input. "
                        "Use `local-path:remote-path` to map an input to a "
                        "specific path remotely",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING_ARRAY),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back("output-path",
                        "Output path to capture. "
                        "--output-path=<path> for each output",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING_ARRAY),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back("output-node-properties",
                        "A list of keys that indicate what additional file "
                        "attributes should be captured "
                        "--output-node-properties=<name> of each property",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING_ARRAY),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back("stdout-file", "Filepath to store stdout",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back("stderr-file", "Filepath to store stderr",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back(
        "platform-properties",
        "The platform requirements for the execution environment "
        "--platform-properties=<key>=<value>,<key>=... for each property",
        TypeInfo(DataType::COMMANDLINE_DT_STRING), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back("environment", "[Deprecated] Use --env instead",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back(
        "env",
        "Environment variables to set in the running program's environment. "
        "To specify multiple ones, use `--env=K1=V1 --env=K2=V2 ...`",
        TypeInfo(DataType::COMMANDLINE_DT_STRING_ARRAY),
        ArgumentSpec::O_OPTIONAL);

    d_spec.emplace_back(
        "exec-timeout",
        "The timeout after which the execution of an Action should be killed "
        "--exec-timeout=<seconds>",
        TypeInfo(DataType::COMMANDLINE_DT_INT), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back("priority",
                        "The priority (relative importance) of this action. A "
                        "priority of 0 means the *default* priority "
                        "--priority=<priority>",
                        TypeInfo(DataType::COMMANDLINE_DT_INT),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back("skip-cache-lookup",
                        "Flag indicating that the server should not check the "
                        "cache when executing an action",
                        TypeInfo(DataType::COMMANDLINE_DT_BOOL),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITHOUT_ARG);

    d_spec.emplace_back(
        "do-not-cache",
        "Flag indicating that ActionResults should not be cached, and "
        "duplicate Action executions will not be merged",
        TypeInfo(DataType::COMMANDLINE_DT_BOOL), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITHOUT_ARG);

    d_spec.emplace_back(
        "follow-symlinks",
        "Follow symlinks when building input trees. "
        "If enabled, each input cannot be circular. Enabled by default",
        TypeInfo(DataType::COMMANDLINE_DT_BOOL), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITHOUT_ARG);
    d_spec.emplace_back("do-not-follow-symlinks", "Do not follow symlinks",
                        TypeInfo(DataType::COMMANDLINE_DT_BOOL),
                        ArgumentSpec::O_OPTIONAL);

    d_spec.emplace_back(
        "correlated-invocations-id",
        "An identifier to tie multiple tool invocations together. "
        "For example, runs of foo_test, bar_test and baz_test on a "
        "post-submit of a given patch. "
        "--correlated-invocations-id=<str>",
        TypeInfo(DataType::COMMANDLINE_DT_STRING), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back(
        "tool-invocation-id",
        "An identifier that ties multiple actions together to a "
        "final result. For example, multiple actions are required"
        " to build and run foo_test. "
        "--tool-invocation-id=<str>",
        TypeInfo(DataType::COMMANDLINE_DT_STRING), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back(
        "tool-name",
        "Additional string to append to the tool-name (trexe) sent in the "
        "metadata. Useful to append additional information about "
        "invocation e.x. python to indicate python was used"
        "--tool-name=<str>",
        TypeInfo(DataType::COMMANDLINE_DT_STRING), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back(
        "tool-version",
        "Additional string to append to the tool-version (e.x. 0.0.1) sent "
        "in the metadata. Useful to append additional information about "
        "invocation e.x. 2.3.4 to append 2.3.4 (version of a package named in"
        "tool-name) to the tool-version metadata"
        "--tool-version=<str>",
        TypeInfo(DataType::COMMANDLINE_DT_STRING), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back(
        "salt",
        "An optional value to place the `Action` into a separate cache "
        "namespace from other instances having the same field contents"
        "--salt=<str>",
        TypeInfo(DataType::COMMANDLINE_DT_STRING), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back(
        "wait",
        "Flag indicating that calls from trexe should be blocking."
        "this is the default behavior",
        TypeInfo(DataType::COMMANDLINE_DT_BOOL), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITHOUT_ARG);

    d_spec.emplace_back(
        "no-wait",
        "Flag indicating that calls from trexe should be async. This"
        "will cause trexe to exit with code 0 upon successful"
        " job submission and write the operation id to `stdout` "
        "in plaintext (e.g. app-cr/6283a9c6-85b2-4b03-92bd)",
        TypeInfo(DataType::COMMANDLINE_DT_BOOL), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITHOUT_ARG);

    d_spec.emplace_back(
        "operation",
        "Operation ID to lookup and download outputs from (to the "
        " specified directory) if -d is set.",
        TypeInfo(DataType::COMMANDLINE_DT_STRING), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back(
        "cancel", "Cancels the specified operation. Requires --operation.",
        TypeInfo(DataType::COMMANDLINE_DT_BOOL), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITHOUT_ARG);

    d_spec.emplace_back("result-metadata-file",
                        "The metadata of trexe execution in JSON will be "
                        "written into this file",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back(
        "action-result-json",
        "The full action result in JSON will be written into this file ",
        TypeInfo(DataType::COMMANDLINE_DT_STRING), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back("cache-only",
                        "If not already cached, execute the action in a local "
                        "BuildBox runner, caching the result",
                        TypeInfo(DataType::COMMANDLINE_DT_BOOL),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITHOUT_ARG);

    d_spec.emplace_back("buildbox-run", "Runner command for local execution",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back(
        "runner-arg",
        "Arguments to pass to buildbox-run for local execution\n"
        "This can be useful if the buildbox-run implementation you're using "
        "supports non-standard options",
        TypeInfo(DataType::COMMANDLINE_DT_STRING_ARRAY),
        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);

    d_spec.emplace_back(
        "stream-logs",
        "Enables live streaming of the remote command's stdout/stderr to the "
        "local stdout/stderr.",
        TypeInfo(DataType::COMMANDLINE_DT_BOOL), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITHOUT_ARG);

    auto loggingSpec = buildboxcommon::loggingCommandLineSpec();

    d_spec.insert(d_spec.end(), loggingSpec.cbegin(), loggingSpec.cend());

    d_spec.emplace_back("", "Command to remote", TypeInfo(&d_command),
                        ArgumentSpec::O_OPTIONAL,
                        ArgumentSpec::C_WITH_REST_OF_ARGS);
}

}; // namespace trexe
