FROM registry.gitlab.com/buildgrid/buildbox/buildbox-common:latest
COPY . /trexe
COPY ./test /home

WORKDIR /trexe

RUN apt-get update && apt-get install jq -y

RUN chmod a+rwx /trexe/test/*.sh && \
    cd /trexe && \
    mkdir build && \
    cd build && \
    cmake -DCMAKE_BUILD_TYPE=DEBUG .. && \
    make install && \
    ctest -V && ls -al /trexe/test
