#!/bin/bash

mkdir -p /tmp/upload/input1;

echo "#!/bin/bash
exit 42;
" > /tmp/upload/input1/hello.sh;

chmod +x /tmp/upload/input1/hello.sh;


OPERATION=$(/usr/local/bin/trexe --remote=http://controller:50051 --no-wait \
                --input-path=/tmp/upload/input1 --output-path=output \
                --result-metadata-file=/tmp/result1.json "./hello.sh");

sleep 5;

/usr/local/bin/trexe --operation="${OPERATION}" --no-wait --d=/home \
    --remote=http://controller:50051 --result-metadata-file=/tmp/result2.json

if [[ "$?" != 42 ]]; then
    exit 1;
fi
